/*-------------------------------------------------------------------------*
 | Copyright (C) 2018 Département Informatique de PolyTech Tours.          |
 |                                                                         |
 | This file is part of PolyShell, yet another shell.                      |
 |                                                                         |
 | PolyShell is free software; you can redistribute it and/or modify       |
 | it under the terms of the GNU General Public License as published by    |
 | the Free Software Foundation; either version 3 of the License,          |
 | or (at your option) any later version.                                  |
 |                                                                         |
 | PolyShell is distributed in the hope that it will be useful,            |
 | but WITHOUT ANY WARRANTY; without even the implied warranty of          |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            |
 | GNU General Public License for more details.                            |
 |                                                                         |
 | You should have received a copy of the GNU General Public License       |
 | along with this program. If not, see <http://www.gnu.org/licenses/>.    |
 |                                                                         |
 | Additional permission under GNU GPL version 3 section 7 ---- If you     |
 | modify PolyShell, or any covered work, by linking or combining it with  |
 | libprovided (or a modified version of that library), containing parts   |
 | covered by the terms of the Creative Commons BY-NC-ND 4.0 International |
 | License, the licensors of  PolyShell grant you additional permission    |
 | to convey the resulting work.                                           |
 *-------------------------------------------------------------------------*/

#include "system/command.h"

// #########################################################################
// #########################################################################
// #########################################################################

#include "tools/macros.h"
#include "tools/dataset/dataset.h"

// #########################################################################
// #########################################################################
// #########################################################################

// -------------------------------------------------------------------------
// --------------------------------- TOOLS ---------------------------------
// -------------------------------------------------------------------------

static Command* __new__(const char *base)
{
    // leak-proof
    Command *cmd = Command_new(base); ck_assert_ptr_ne(cmd, NULL); return cmd;
}

static int __cmd_equals__(const Command *cmd1, const Command *cmd2)
{
    if(cmd1->status              != cmd2->status             ) return 1;
    if(cmd1->base                != cmd2->base               ) return 1;
    if(cmd1->options             != cmd2->options            ) return 1;
    if(cmd1->nbOptions           != cmd2->nbOptions          ) return 1;
    if(cmd1->capacityOption      != cmd2->capacityOption     ) return 1;
    if(cmd1->redirections[0]     != cmd2->redirections[0]    ) return 1;
    if(cmd1->redirections[1]     != cmd2->redirections[1]    ) return 1;
    if(cmd1->redirections[2]     != cmd2->redirections[2]    ) return 1;
    if(cmd1->redirectionTypes[0] != cmd2->redirectionTypes[0]) return 1;
    if(cmd1->redirectionTypes[1] != cmd2->redirectionTypes[1]) return 1;
    if(cmd1->redirectionTypes[2] != cmd2->redirectionTypes[2]) return 1;
    if(cmd1->next                != cmd2->next               ) return 1;
    if(cmd1->prev                != cmd2->prev               ) return 1;
    return 0;
}

static void __check_init__(const char *base)
{
    const          int su = strcmp(base, "su") == 0;
    const unsigned int nbOptions = su ? 3 : 1;
    Command *cmd = __new__(base);
    {
        // any command
        ck_assert_int_ne (cmd->status              ,       0   );
        ck_assert_ptr_ne (cmd->base                ,      base );
        ck_assert_str_eq (cmd->base                ,      base );
        ck_assert_ptr_ne (cmd->options             ,      NULL );
        ck_assert_uint_eq(cmd->nbOptions           , nbOptions );
        ck_assert_uint_ge(cmd->capacityOption      , nbOptions );
        ck_assert_ptr_ne (cmd->options[0]          ,      base );
        ck_assert_ptr_ne (cmd->options[0]          , cmd->base );
        ck_assert_str_eq (cmd->options[0]          ,      base );
        ck_assert_ptr_eq (cmd->redirections[0]     ,      NULL );
        ck_assert_ptr_eq (cmd->redirections[1]     ,      NULL );
        ck_assert_ptr_eq (cmd->redirections[1]     ,      NULL );
        ck_assert        (cmd->redirectionTypes[0] == UNDEFINED);
        ck_assert        (cmd->redirectionTypes[1] == UNDEFINED);
        ck_assert        (cmd->redirectionTypes[2] == UNDEFINED);
        ck_assert_ptr_eq (cmd->next                ,      NULL );
        ck_assert_ptr_eq (cmd->prev                ,      NULL );
        // su only
        if(su) { ck_assert_ptr_ne (cmd->options[1],      "-s"         );
                 ck_assert_str_eq (cmd->options[1],      "-s"         );
                 ck_assert_ptr_ne (cmd->options[2], Configuration.exec);
                 ck_assert_str_eq (cmd->options[2], Configuration.exec); }
    }
    Command_flushLivingCommands();
}

static void __check_redirect__( // NORMAL or APPEND or FUSION only
                                RedirectionType type )
{
    if(    type != NORMAL
        && type != APPEND
        && type != FUSION ) ck_abort_msg("type not supported");
    Command *cmd = __new__("solver");
    Command  cpy = *cmd;
    {
        const char *fichiers[2][3] = { { "/home/lara/in.txt", "/home/lara/out.txt", "/home/lara/error.txt" },
                                       { "/home/toto/in.txt", "/home/toto/out.txt", "/home/toto/error.txt" }  };
        for(int it = 0; it < 2; ++it)
        {
            for(int fd = type == NORMAL ? 0 :
                         type == APPEND ? 1 :
                                          2 ; fd < 3; ++fd)
            {
                Command *ret = type == NORMAL ? Command_redirect      (cmd, fd, fichiers[it][fd]) :
                               type == APPEND ? Command_appendRedirect(cmd, fd, fichiers[it][fd]) :
                                                Command_mergeOutputs  (cmd                      ) ;
                // cmd status redirectionTypes
                ck_assert_ptr_eq(cmd                       ,  ret );
                ck_assert_int_ne(cmd->status               ,   0  );
                ck_assert       (cmd->redirectionTypes[fd] == type);
                // redirections
                if(    type
                    == FUSION ) { ck_assert_ptr_eq(cmd->redirections[fd],      NULL       ); }
                           else { ck_assert_ptr_ne(cmd->redirections[fd], fichiers[it][fd]);
                                  ck_assert_str_eq(cmd->redirections[fd], fichiers[it][fd]); }
            } }
        // other attributes
        for(int fd = 0; fd < 3; ++fd) { cpy.redirections    [fd] = cmd->redirections    [fd];
                                        cpy.redirectionTypes[fd] = cmd->redirectionTypes[fd]; }
        ck_assert_msg( __cmd_equals__(cmd, &cpy) == 0,
                       "unexpected attributes have been modified by %s", type == NORMAL ? "Command_redirect"       :
                                                                         type == APPEND ? "Command_appendRedirect" :
                                                                                          "Command_mergeOutputs"     );
    }
    Command_flushLivingCommands();
}

static void __check_add_option__(const char *arg, int expend, const char **files, const char **folders, const char **binaries, const char **result)
{

    Command *cmd = __new__("notsu"),
             cpy = *cmd;

// ------------------------------- easy case -------------------------------
// -------------------------------------------------------------------------

    if(    !expend
        || !arg )
    {
        // add option
        Command *ret = Command_addOption(cmd, arg, expend);
        ck_assert_ptr_eq(cmd, ret);
        // check the result
                  ck_assert_uint_eq(cmd->nbOptions     ,  2  );
                  ck_assert_uint_ge(cmd->capacityOption,  2  );
        if(arg) { ck_assert_ptr_ne (cmd->options[1]    , arg );
                  ck_assert_str_eq (cmd->options[1]    , arg ); }
           else { ck_assert_ptr_eq (cmd->options[1]    , NULL); }
    }

// ------------------------------- hard case -------------------------------
// -------------------------------------------------------------------------

    else
    {

        // arguments
        if( !files    || !folders
         || !binaries || !result  )
              fatalError("invalid argument");

        // dataset
        safeNEW(FsTestingSet*, dataset, 1, FsTestingSet_delete, FsTestingSet_new(files, folders, binaries));
        if(!dataset[0])
              fatalError("FsTestingSet_new has failed");

        // chdir to dataset.path
        if(chdir(dataset[0]->path))
              fatalError("chdir has failed");

        // nbOptions
        unsigned int size = 0;
        for(const char **ptr = result; *ptr; ++ptr, ++size);

        // add option
        Command *ret = Command_addOption(cmd, arg, expend);
        ck_assert_ptr_eq(cmd, ret);

        // number of options
        ck_assert_uint_eq(cmd->nbOptions     , 1U+size);
        ck_assert_uint_ge(cmd->capacityOption, 1U+size);

        // duplicate bin
        safeMALLOC(char*, bin, 1, duplicateString( dataset[0]->bin
                                                   // remove parent directory             // remove trailing /
                                                   + stringLength(dataset[0]->path) ));   bin[0][stringLength(bin[0])-1] = '\0';

        // results
        for(unsigned int ires = 0; ires < size; ++ires)
        {

            int found = 0; const char *tmp;

            // replace $bin$ by dataset.bin if necessary
            safeMALLOC(char*, elem, 1, (tmp=startWith(result[ires], "$bin$")) ? concatenateStrings(bin[0], tmp, 0)
                                                                              : duplicateString(result[ires] ));

            // check that elem has been set correctly
            if(!elem[0])
                  fatalError("duplicateString or concatenateStrings has failed");

            for(unsigned int iopt = 1; iopt < cmd->nbOptions; ++iopt)
            {

                // remove ./ if necessary
                const char *opt = (tmp=startWith(cmd->options[iopt], "./")) ? tmp
                                                                            : cmd->options[iopt];

                // search each elem of result in cmd->options
                if( !strcmp(elem[0],
                            opt       )) ++found;

            }

            ck_assert_msg( found == 1U,
                           "%s has not been found or has been found twice", elem );

            safeDELETE(elem);

        }

        // deallocate memory
        safeDELETE(bin);
        safeDELETE(dataset);

    }

// --------------------------------- both ----------------------------------
// -------------------------------------------------------------------------

    cpy.nbOptions      = cmd->nbOptions     ;
    cpy.capacityOption = cmd->capacityOption;
    cpy.options        = cmd->options       ;
    ck_assert_msg( __cmd_equals__(cmd, &cpy) == 0,
                   "unexpected attributes have been modified by Command_addOption" );

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

    Command_flushLivingCommands();

}

// -------------------------------------------------------------------------
// --------------------------------- TESTS ---------------------------------
// -------------------------------------------------------------------------

START_TEST(test_init)
{
    __check_init__("ls");
}
END_TEST

START_TEST(test_su)
{
    __check_init__("su");
}
END_TEST

START_TEST(test_living_cmds)
{
    Command* cmds[1024];
    {
        for(unsigned int i = 0; i < 1024; ++i) { cmds[i] = __new__("any"); ck_assert_uint_eq(livingCommandsSize    ,  i+1   );
                                                                           ck_assert_uint_ge(livingCommandsCapacity,  i+1   );
                                                                           ck_assert_ptr_eq (livingCommands[i]     , cmds[i]); }
    }
    Command_flushLivingCommands();
    ck_assert_uint_eq(livingCommandsSize, 0);
}
END_TEST

START_TEST(test_redirect)
{
    __check_redirect__(NORMAL);
}
END_TEST

START_TEST(test_appendRedirect)
{
    __check_redirect__(APPEND);
}
END_TEST

START_TEST(test_mergeOutputs)
{
    __check_redirect__(FUSION);
}
END_TEST

START_TEST(test_pipe)
{
    Command *cmd1 = __new__("cmd1"),
            *cmd2 = __new__("cmd2");
    {
        close(STDERR_FILENO);
        Command *ret = Command_pipe(cmd1, cmd2); ck_assert_uint_ne(cmd1->status, 0); ck_assert_ptr_eq(cmd1->next, cmd2);
                                                 ck_assert_uint_ne(cmd2->status, 0); ck_assert_ptr_eq(cmd2->prev, cmd1); ck_assert_ptr_eq(ret, cmd2);
                 ret = Command_pipe(cmd1, cmd2); ck_assert_uint_eq(cmd1->status, 0);
                                                 ck_assert_uint_eq(cmd2->status, 0);                                     ck_assert_ptr_eq(ret, cmd2);
    }
    Command_flushLivingCommands();
}
END_TEST

START_TEST(test_nb_members)
{
    Command *cmd1 = __new__("cmd1"),
            *cmd2 = __new__("cmd2"),
            *cmd3 = __new__("cmd2"),
            *cmd4 = __new__("cmd2"),
            *cmd5 = __new__("cmd2");
    {
        Command_pipe(Command_pipe(Command_pipe(Command_pipe( // cmd1 | cmd2 | ... | cmd5
                                                             cmd1, cmd2), cmd3), cmd4), cmd5);
        ck_assert_uint_eq(Command_getNbMember(cmd1), 5);
        ck_assert_uint_eq(Command_getNbMember(cmd2), 5);
        ck_assert_uint_eq(Command_getNbMember(cmd3), 5);
        ck_assert_uint_eq(Command_getNbMember(cmd4), 5);
        ck_assert_uint_eq(Command_getNbMember(cmd5), 5);
    }
    Command_flushLivingCommands();
}
END_TEST

START_TEST(test_add_opt_l1)
{
    __check_add_option__("argument", 0, NULL, NULL, NULL, NULL);
    __check_add_option__("arg*me*t", 0, NULL, NULL, NULL, NULL);
    __check_add_option__(NULL      , 0, NULL, NULL, NULL, NULL);
    __check_add_option__(NULL      , 1, NULL, NULL, NULL, NULL);
}
END_TEST

START_TEST(test_add_opt_l2)
{
    const char *f[] = { "data_1.txt", "data_2.txt", "results.txt", "mkmyjob.sh", "blagues.doc", ".secret", NULL },
               *d[] = { "documents", "desktop", ".ssh", NULL }, *b[] = { "find", "mkfifo", "mkdir", "mkfontdir", NULL };
    // 1
    const char *arg1 = "*", *r1[] = { "data_1.txt", "data_2.txt", "results.txt", "mkmyjob.sh", "blagues.doc", "documents", "desktop", "$bin$", ".secret", ".ssh", NULL };
    __check_add_option__(arg1, 1, f, d, b, r1);
    // 2
    const char *arg2 = "d*", *r2[] = { "data_1.txt", "data_2.txt", "documents", "desktop", NULL };
    __check_add_option__(arg2, 1, f, d, b, r2);
    // 3
    const char *arg3 = "*.txt", *r3[] = { "data_1.txt", "data_2.txt", "results.txt", NULL };
    __check_add_option__(arg3, 1, f, d, b, r3);
}
END_TEST

START_TEST(test_add_opt_l3)
{
    const char *f[] = { "data_1.txt", "data_2.txt", "results.txt", "mkmyjob.sh", "blagues.doc", ".secret", NULL },
               *d[] = { "documents", "desktop", ".ssh", NULL }, *b[] = { "find", "mkfifo", "mkdir", "mkfontdir", NULL };
    // 1
    const char *arg1 = "bin.*/m*", *r1[] = { "$bin$/mkfifo", "$bin$/mkdir", "$bin$/mkfontdir", NULL };
    __check_add_option__(arg1, 1, f, d, b, r1);
    // 2
    const char *arg2 = ".*", *r2[] = { ".secret", ".ssh", NULL };
    __check_add_option__(arg2, 1, f, d, b, r2);
    // 3
    const char *arg3 = "bin.*/*fi*", *r3[] = { "$bin$/find", "$bin$/mkfifo", NULL };
    __check_add_option__(arg3, 1, f, d, b, r3);
}
END_TEST

START_TEST(test_status)
{
    Command *cmd1 = __new__("cmd1"),
            *cmd2 = __new__("cmd2"),
             cpy1 = *cmd1          ;
    // cmd2 is dangerous
    cmd2->status = cpy1.status = 0;
    {
        Command_pipe          (cmd1, cmd2         ); ck_assert_msg(__cmd_equals__(cmd1, &cpy1) == 0, "status is not correctly propagated in Command_pipe");
        Command_redirect      (cmd1, 0, "anything"); ck_assert_msg(__cmd_equals__(cmd1, &cpy1) == 0, "status is ignored by Command_redirect"             );
        Command_redirect      (cmd1, 1, "anything"); ck_assert_msg(__cmd_equals__(cmd1, &cpy1) == 0, "status is ignored by Command_redirect"             );
        Command_redirect      (cmd1, 2, "anything"); ck_assert_msg(__cmd_equals__(cmd1, &cpy1) == 0, "status is ignored by Command_redirect"             );
        Command_appendRedirect(cmd1, 1, "anything"); ck_assert_msg(__cmd_equals__(cmd1, &cpy1) == 0, "status is ignored by Command_appendRedirect"       );
        Command_appendRedirect(cmd1, 2, "anything"); ck_assert_msg(__cmd_equals__(cmd1, &cpy1) == 0, "status is ignored by Command_appendRedirect"       );
        Command_mergeOutputs  (cmd1               ); ck_assert_msg(__cmd_equals__(cmd1, &cpy1) == 0, "status is ignored by Command_mergeOutputs"         );
        Command_addOption     (cmd1, "args", 0    ); ck_assert_msg(__cmd_equals__(cmd1, &cpy1) == 0, "status is ignored by Command_addOption"            );
        Command_addOption     (cmd1, "args", 1    ); ck_assert_msg(__cmd_equals__(cmd1, &cpy1) == 0, "status is ignored by Command_addOption"            );
        Command_addOption     (cmd1, "ar*s", 1    ); ck_assert_msg(__cmd_equals__(cmd1, &cpy1) == 0, "status is ignored by Command_addOption"            );
                                                     ck_assert_msg(Command_getNbMember(cmd1)   == 0, "status is ignored by Command_getNbMember"          );
    }
    Command_flushLivingCommands();
}
END_TEST

// #########################################################################
// #########################################################################
// #########################################################################

START_SUITE_BUILDER(command)
{

    ONE_TEST_CASE(init          , test_init          );
    ONE_TEST_CASE(su            , test_su            );
    ONE_TEST_CASE(living_cmds   , test_living_cmds   );
    ONE_TEST_CASE(redirect      , test_redirect      );
    ONE_TEST_CASE(appendRedirect, test_appendRedirect);
    ONE_TEST_CASE(mergeOutputs  , test_mergeOutputs  );
    ONE_TEST_CASE(pipe          , test_pipe          );
    ONE_TEST_CASE(nb_members    , test_nb_members    );
    ONE_TEST_CASE(add_opt_l1    , test_add_opt_l1    );
    ONE_TEST_CASE(add_opt_l2    , test_add_opt_l2    );
    ONE_TEST_CASE(add_opt_l3    , test_add_opt_l3    );
    ONE_TEST_CASE(staus         , test_status        );

}
END_SUITE_BUILDER
