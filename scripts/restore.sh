#!/bin/bash
source ./scripts/config
prerequisite tar
prerequisite mktemp
prerequisite curl
start
tempdir=$(mktemp --directory)
curl -s "$URL/upload.php?username=$user&password=$pass" \
     -o $tempdir/backup.tar.gz \
     -w "X_RESPONDE_CODE_X=%{response_code}" | grep "X_RESPONDE_CODE_X=200" > /dev/null 2>&1 || quit "Authentication has failed." "Abort."
rm -rf backup/ && mkdir backup/
cp $(find src/ -iname *.etu.?) backup/
tar -xzf $tempdir/backup.tar.gz || quit "An error occured while extracting backup.tar.gz." "Abort."
rm -rf $tempdir/
end
print "Your project has been updated successfully."
