/*-------------------------------------------------------------------------*
 | Copyright (C) 2018 Département Informatique de PolyTech Tours.          |
 |                                                                         |
 | This file is part of PolyShell, yet another shell.                      |
 |                                                                         |
 | PolyShell is free software; you can redistribute it and/or modify       |
 | it under the terms of the GNU General Public License as published by    |
 | the Free Software Foundation; either version 3 of the License,          |
 | or (at your option) any later version.                                  |
 |                                                                         |
 | PolyShell is distributed in the hope that it will be useful,            |
 | but WITHOUT ANY WARRANTY; without even the implied warranty of          |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            |
 | GNU General Public License for more details.                            |
 |                                                                         |
 | You should have received a copy of the GNU General Public License       |
 | along with this program. If not, see <http://www.gnu.org/licenses/>.    |
 *-------------------------------------------------------------------------*/

#include "interactive/all.h"
#include "misc/string.h"

// #########################################################################
// #########################################################################
// #########################################################################

Input* IMPLEMENT(Input_new)(void)
{
    Input *input= malloc(sizeof(Input));
	if(input){
		if(Input_init(input)){
			free(input);
			return NULL;
		}
	}
    return input;
}

int IMPLEMENT(Input_init)(Input *input)
{
	input->pos = 0;
	input->current = NULL;
	return 0;
}

void IMPLEMENT(Input_delete)(Input *input)
{
   if(input){
		Input_finalize(input);
		free(input);
	}
}

void IMPLEMENT(Input_finalize)(Input *input)
{
	Input_clear(input);
}

void IMPLEMENT(Input_clear)(Input *input)
{
    if(input->current){
		Cell * prev = input->current->previous;
		while(prev != NULL){
			Cell * tmp = prev->previous;
			Cell_delete(prev);
			prev = tmp;
		}
		Cell * nex = input->current->next;
		while(nex != NULL){
			Cell * tmp = nex->next;
			Cell_delete(nex);
			nex = tmp;
		}
		Cell_delete(input->current);
		input->current = NULL;
		input->pos = 0;
	}
}

size_t IMPLEMENT(Input_size)(const Input *input)
{
	Cell * act = input->current;
	size_t taille;
	if(act){
		if(&act->bucket){ taille = Bucket_size(&act->bucket); }
		while (act->next != NULL){
			act = act->next;
			if(&act->bucket){ taille = taille + Bucket_size(&act->bucket); }
		}
		act = input->current;
		while(act->previous != NULL){
			act = act->previous;
			if(&act->bucket){ taille = taille + Bucket_size(&act->bucket); }
		}
		return taille;
	}
	else{
		return 0;
	}
}

int IMPLEMENT(Input_insert)(Input *input, char c)
{
	if(input->current == NULL){
		Cell *newCell = Cell_new();
		if(newCell == NULL){ return -1; }
		Bucket_insert(&newCell->bucket, 0, c);
		input->current = newCell;
		input->pos = 1;
	}
	else{
		if(Bucket_full(&input->current->bucket)){
			Cell * newCell = Cell_new();
			if(newCell == NULL){ return -1;}
			Cell_insertAfter(input->current, newCell);
			if(input->pos == input->current->bucket.top +1){
				input->current = newCell;
				input->pos = 0;
			}
			else{
				Bucket_move(&input->current->bucket, input->pos, &newCell->bucket);
			}
		}
		Bucket_insert(&input->current->bucket, input->pos, c);
		Input_moveRight(input);
	}
	return 0;
}

int IMPLEMENT(Input_backspace)(Input *input)
{
	/*
	if((input->pos > 0 && input->pos <= input->current->bucket.top + 1) || (input->pos == 0 && input->current->previous)){
		Input_moveLeft(input);
		Input_del(input);
		return 0;
	}
	return 1;
    //*/return provided_Input_backspace(input);
	
}

int IMPLEMENT(Input_del)(Input *input)
{
	if(input->current != NULL && input->pos >= 0 && input->pos <= input->current->bucket.top) {
		Bucket_remove(&input->current->bucket, input->pos);
		if(Bucket_empty(&input->current->bucket)){
			Cell * actC = input->current;
			Cell * prev;
			Cell * suiv;
			if (input->current->next && input->current->previous){
				prev = input->current->previous;
				suiv = input->current->next;
				prev->next = suiv;
				suiv->previous = prev;
				input->current = suiv;
				input->pos = 0;
				Cell_delete(actC);
			}
			else if(input->current->next && input->current->previous == NULL){
				suiv = input->current->next;
				suiv->previous = NULL;
				input->current = suiv;
				input->pos = 0;
				Cell_delete(actC);
			}
			else if(input->current->next == NULL && input->current->previous){
				prev = input->current->previous;
				prev->next = NULL;
				input->current = prev;
				input->pos = input->current->bucket.top + 1;
				Cell_delete(actC);
			}
		}
		else if(!Bucket_empty(&input->current->bucket) && input->pos == input->current->bucket.top + 1){
			if(input->current->next){
				input->current = input->current->next;
				input->pos = 0;
			}
		}
		return 0;
	}
	return 1;
}

int IMPLEMENT(Input_moveLeft)(Input *input)
{
	if(input->current){
		if(input->pos != 0){ --input->pos; }
		else{
			Cell * act = input->current->previous;
			if(act){
				input->current = act;
				input->pos = act->bucket.top;
			}
			else{
				return 1;
			}
		}
		return 0;
	}
	return 1;
}

int IMPLEMENT(Input_moveRight)(Input *input)
{
	if(input->current){
		if(input->pos >= -1 && input->pos < input->current->bucket.top){
			++input->pos;
		}
		else if(input->pos == input->current->bucket.top && input->current->next != NULL){
			input->current = input->current->next;
			input->pos = 0;
		}
		else if(input->pos == input->current->bucket.top && input->current->next == NULL){
			input->pos = input->current->bucket.top + 1;
		}
		else if(input->pos == input->current->bucket.top + 1 && input->current->next == NULL){
			return 1;
		}
	}
	return 0;
}

int IMPLEMENT(Input_equals)(const Input *x, const Input *other)
{
    return (x->current == other->current && x->pos == other->pos);
}

char* IMPLEMENT(Input_toString)(const Input *input)
{
	if(input){
		InputIterator *inteinput= malloc(sizeof(InputIterator));
		InputIterator_initIterator(input, inteinput);
		size_t taille = Input_size(input);
		unsigned int i;
		char *tmp = malloc(sizeof(char) * taille + 1);
		for(i = 0; i < taille; ++i){
			tmp[i] = InputIterator_get(inteinput);
			InputIterator_next(inteinput);
		}
		tmp[i]= '\0';
		free(inteinput);
		return tmp;
	}
	return NULL;
}

void IMPLEMENT(InputIterator_initIterator)(const Input *input, InputIterator *inputIterator)
{
	*inputIterator = *input;
	while(!Input_moveLeft(inputIterator));
}

int IMPLEMENT(InputIterator_hasNext)(const InputIterator *inputIterator)
{
    return inputIterator->current != NULL && (inputIterator->current->next != NULL || inputIterator->pos <= inputIterator->current->bucket.top);
}

void IMPLEMENT(InputIterator_next)(InputIterator *inputIterator)
{
    Input_moveRight(inputIterator);
}

char IMPLEMENT(InputIterator_get)(const InputIterator *inputIterator)
{
    return inputIterator->current->bucket.content[inputIterator->pos];
}

int IMPLEMENT(Input_load)(Input *input, const char *cmd)
{
	if(input && cmd){
		Input_clear(input);
		for(unsigned int i = 0; i < stringLength(cmd); ++i){
			Input_insert(input, cmd[i]);
		}
		return 0;
	}
	return 1;
}

char* IMPLEMENT(Input_getEditedWord)(const Input *input)
{
	if(input){
		char *tmp = malloc(sizeof(char) * Input_size(input));
		int depart = input->pos - 1; int i = 0;
		if(depart < 0 && input->current->previous == NULL){
            free(tmp);
			return NULL;
		}
		while(depart >= 0 && input->current->bucket.content[depart] != ' '){
			tmp[i] = input->current->bucket.content[depart];
			--depart;
			++i;
		}
		tmp[i] = '\0';
		tmp = mkReverse(tmp, -1);
		return tmp;
	}
	return NULL;
    //*/return provided_Input_getEditedWord(input);
}
