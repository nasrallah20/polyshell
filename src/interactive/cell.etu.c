/*-------------------------------------------------------------------------*
 | Copyright (C) 2018 Département Informatique de PolyTech Tours.          |
 |                                                                         |
 | This file is part of PolyShell, yet another shell.                      |
 |                                                                         |
 | PolyShell is free software; you can redistribute it and/or modify       |
 | it under the terms of the GNU General Public License as published by    |
 | the Free Software Foundation; either version 3 of the License,          |
 | or (at your option) any later version.                                  |
 |                                                                         |
 | PolyShell is distributed in the hope that it will be useful,            |
 | but WITHOUT ANY WARRANTY; without even the implied warranty of          |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            |
 | GNU General Public License for more details.                            |
 |                                                                         |
 | You should have received a copy of the GNU General Public License       |
 | along with this program. If not, see <http://www.gnu.org/licenses/>.    |
 *-------------------------------------------------------------------------*/

#include "interactive/cell.h"
#include <assert.h>

// #########################################################################
// #########################################################################
// #########################################################################

Cell* IMPLEMENT(Cell_new)(void)
{
    Cell *cell= malloc(sizeof(Cell));
	if(cell){
		if(Cell_init(cell)){
			free(cell);
			cell = NULL;
		}
	}
    return cell;
}

int IMPLEMENT(Cell_init)(Cell *cell)
{
    cell->next = NULL;
	cell->previous = NULL;
	if(Bucket_init(&cell->bucket)) { return -1; }
	return 0;
}

void IMPLEMENT(Cell_delete)(Cell *cell)
{
   if(cell){
		Cell_finalize(cell);
		free(cell);
	}
}

void IMPLEMENT(Cell_finalize)(Cell *cell)
{
    Bucket_finalize(&cell->bucket);
	cell->next = cell->previous = NULL;
}

void IMPLEMENT(Cell_insertAfter)(Cell *cell, Cell *newCell)
{
	assert(cell != NULL && newCell != NULL && newCell->next == NULL && newCell->previous == NULL);
	newCell->previous = cell;
	newCell->next = cell->next;
	if(cell->next != NULL){
		cell->next->previous = newCell;
	}
	cell->next = newCell;
}
