/*-------------------------------------------------------------------------*
 | Copyright (C) 2018 Département Informatique de PolyTech Tours.          |
 |                                                                         |
 | This file is part of PolyShell, yet another shell.                      |
 |                                                                         |
 | PolyShell is free software; you can redistribute it and/or modify       |
 | it under the terms of the GNU General Public License as published by    |
 | the Free Software Foundation; either version 3 of the License,          |
 | or (at your option) any later version.                                  |
 |                                                                         |
 | PolyShell is distributed in the hope that it will be useful,            |
 | but WITHOUT ANY WARRANTY; without even the implied warranty of          |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            |
 | GNU General Public License for more details.                            |
 |                                                                         |
 | You should have received a copy of the GNU General Public License       |
 | along with this program. If not, see <http://www.gnu.org/licenses/>.    |
 *-------------------------------------------------------------------------*/

#include "interactive/autocomplete.h"
#include "system/info.h"
#include "misc/filesystem.h"
#include "misc/ferror.h"
#include "misc/string.h"
#include "misc/regex.h"
#include <stdlib.h>
#include <stdio.h>
// #########################################################################
// #########################################################################
// #########################################################################

char* IMPLEMENT(prependHomeDir)(char *str){
    if(    str
        && str[0] == '~' )
    {
        char *nstr = NULL;
        // ~ or ~/xxx
        if ( str[1] == '\0' || str[1] ==  '/' )
        {
            const char *homedir; userInformation(NULL, &homedir, NULL);
            nstr = concatenateStrings(homedir, str+1, 0);
        }
        // ~~ or ~~/xxx
        else if ( str[1] == '~' && ( str[2] == '/' || str[2] == '\0' ) )
        {
            nstr = duplicateString(str+1);
        }
        // replace str by nstr if necessary and if concatenateStrings/duplicateString ran successfully
        if (nstr) { free(str);
                    str = nstr; }
    }
    return str;
}

int autocomplete_normal(char * lastprefix, FolderIterator * IteratorDir, Fifo * FifoDir, char **extend){
	unsigned int NbSug = 0;
	while(FolderIterator_hasNext(IteratorDir)){
		if(startWith(FolderIterator_get(IteratorDir), lastprefix)){
			char * nomF = (char *) FolderIterator_get(IteratorDir);
			if(!Fifo_full(FifoDir)){
				if(FolderIterator_isDir(IteratorDir)){
					char * ttmmpp = concatenateStrings(nomF, "/", 0);
					Fifo_push(FifoDir, ttmmpp);
					free(ttmmpp);
				}
				else{
					Fifo_push(FifoDir, nomF);
				}
			}
			++NbSug;
			if(*extend == NULL){
				const char * tttmmmppp = startWith(FolderIterator_get(IteratorDir), lastprefix);
				*extend = duplicateString(tttmmmppp);
			}
			else{
				mkCommon(*extend, startWith(FolderIterator_get(IteratorDir), lastprefix));
			}
		}
		FolderIterator_next(IteratorDir);
	}
	FolderIterator_delete(IteratorDir);
	return NbSug;
}



int IMPLEMENT(autocomplete)(const char *prefix, unsigned int limit, unsigned int *nbItems, char **extend, Fifo **results)
{	
	if(prefix && limit != 0 && nbItems && extend && results){
		if(stringCompare(prefix, ".") == 0 || stringCompare(prefix, "..") == 0 || stringCompare(prefix, "~") == 0){
			unsigned int NbSug = 1;
			*nbItems = NbSug;
			*extend = duplicateString("/");
			*results = NULL;
			return 0;
		}
		const char* CurrentDir = getCurrentDirectory(0);
		char * lastprefix = duplicateString(prefix);
		char * lastDir;
		char * pos = NULL;
		
		int NbSug = 0;
		FifoMode mode = COMPOSE;
		Fifo * FifoDir;
		FolderIterator * IteratorDir;
		FifoDir = Fifo_new(limit, mode);
		
		if(!belongs('*', lastprefix)){
			if(belongs('/', prefix)){
				char * prefixtemp = prependHomeDir(lastprefix);
				char * prefixtempbis = getRealString(prefixtemp, '/' , &pos);
				char * lastprefixtmp = findLast((char *)prefixtempbis, '/') + 1;
				lastDir = subString(prefixtempbis, stringLength(prefixtempbis) - stringLength(lastprefixtmp));
				free(lastprefix);
				lastprefix = duplicateString(lastprefixtmp);
				free(prefixtempbis);
			}
			else{
				lastDir = duplicateString(CurrentDir);
				
				const char * DirPath = getPath();
				IteratorDir = FolderIterator_new(DirPath, 1);
				if(IteratorDir == NULL){
					FolderIterator_delete(IteratorDir);
				}
				else{
					int NbPath = autocomplete_normal(lastprefix, IteratorDir, FifoDir, extend);
					NbSug = NbSug + NbPath;
				}
				
			}
			
			IteratorDir = FolderIterator_new(lastDir, 1);
			if(IteratorDir == NULL){				
				free(lastDir);
				free(lastprefix);
				Fifo_delete(FifoDir);
				return 1;
			}
			
			NbSug = NbSug + autocomplete_normal(lastprefix, IteratorDir, FifoDir, extend);
		}
		else{
			char * FistNotEsc = NULL;
			char * tmp = getRealString(lastprefix, '*', &FistNotEsc);
			free(lastprefix);
			lastprefix = duplicateString(tmp);
			lastDir = duplicateString(CurrentDir);
			if(FistNotEsc != NULL){	
				free(lastprefix);
				free(tmp);
				free(lastDir);
				Fifo_delete(FifoDir);
				return 1;
			}
			IteratorDir = FolderIterator_new(lastDir, 1);
			if(IteratorDir == NULL){		
				free(lastprefix);
				free(tmp);
				free(lastDir);
				Fifo_delete(FifoDir);
				return 1;
			}
			
			NbSug = autocomplete_normal(tmp, IteratorDir, FifoDir, extend);
			
			free(tmp);
		
		}
		*nbItems = NbSug;
		if(NbSug == 0){
			free(*extend);
			*extend = NULL;
			*results = NULL;
			Fifo_delete(FifoDir);
			free(lastprefix);
			free(lastDir);
			return 0;
		}
		else if(NbSug == 1){
			free(*extend);
			*extend = duplicateString(startWith(Fifo_front(FifoDir), lastprefix));
			*results = NULL;
			Fifo_delete(FifoDir);
			free(lastprefix);
			free(lastDir);
			return 0;
		}
		else{
			if(**extend){
				*results = NULL;
				Fifo_delete(FifoDir);
			}
			else{
				free(*extend);
				*extend = NULL;
				*results = FifoDir;
			}
			free(lastprefix);
			free(lastDir);
			return 0;
		}
		Fifo_delete(FifoDir);
		free(lastprefix);
		free(lastDir);
		Fifo_delete(FifoDir);
	}
	return 1;
	
	
    //*/return provided_autocomplete(prefix, limit, nbItems, extend, results);
}