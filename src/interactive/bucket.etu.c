/*-------------------------------------------------------------------------*
 | Copyright (C) 2018 Département Informatique de PolyTech Tours.          |
 |                                                                         |
 | This file is part of PolyShell, yet another shell.                      |
 |                                                                         |
 | PolyShell is free software; you can redistribute it and/or modify       |
 | it under the terms of the GNU General Public License as published by    |
 | the Free Software Foundation; either version 3 of the License,          |
 | or (at your option) any later version.                                  |
 |                                                                         |
 | PolyShell is distributed in the hope that it will be useful,            |
 | but WITHOUT ANY WARRANTY; without even the implied warranty of          |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            |
 | GNU General Public License for more details.                            |
 |                                                                         |
 | You should have received a copy of the GNU General Public License       |
 | along with this program. If not, see <http://www.gnu.org/licenses/>.    |
 *-------------------------------------------------------------------------*/

#include "interactive/bucket.h"
#include <assert.h>

// #########################################################################
// #########################################################################
// #########################################################################

Bucket* IMPLEMENT(Bucket_new)(void)
{
	Bucket *bucket= malloc(sizeof(Bucket));
	if(bucket){
		if(Bucket_init(bucket)){
			free(bucket);
			return NULL;
		}
	}
    return bucket;
	//*/return provided_Bucket_new();
}

int IMPLEMENT(Bucket_init)(Bucket *bucket)
{
	bucket->top = -1;
	return 0;
}

void IMPLEMENT(Bucket_delete)(Bucket *bucket)
{
	if(bucket){
		Bucket_finalize(bucket);
		free(bucket);
	}
}

void IMPLEMENT(Bucket_finalize)(Bucket *bucket)
{
	(void) bucket; // <- EVITE LE WARNING / ERROR DE NON UTILISATION !!! MAIS NE FAIT RIEN !!!
	//provided_Bucket_finalize(bucket);
}

size_t IMPLEMENT(Bucket_size)(const Bucket *bucket)
{
	return (size_t) (bucket-> top + 1);
}

// #define NDEBUG // <- ignorer tous les asserts en mode release uniquement
void IMPLEMENT(Bucket_remove)(Bucket *bucket, int position)
{
	assert(bucket != NULL && position >=0 && position <= bucket->top);
	if(position == bucket->top){
		bucket->top = bucket->top - 1;
	}
	else if (position < bucket->top && position >= 0){
		for(int i = position; i < bucket->top ; ++i){
			bucket->content[i] = bucket->content[i + 1];
		}
		bucket->top = bucket->top - 1;
	}
}

void IMPLEMENT(Bucket_insert)(Bucket *bucket, int position, char c)
{
	if(Bucket_empty(bucket)){ 
		bucket->content[0] = c; 
		bucket->top = bucket->top + 1;
	}
    else if(!(Bucket_full(bucket))){
		bucket->top = bucket->top + 1;
		for(int i = bucket->top; i > position ; --i){
			bucket->content[i] = bucket->content[i - 1];
		}
		bucket->content[position] = c;
	}
}

void IMPLEMENT(Bucket_move)(Bucket *from, int position, Bucket *to)
{
    if(!Bucket_empty(from) && position >= 0 && position <= from->top){
		int j = 0;
		for(int i = position; i <= from->top; ++i){
			Bucket_insert(to,j,from->content[i]);
			++j;
		}
		from->top = position - 1;
	}
}

int IMPLEMENT(Bucket_empty)(const Bucket *bucket)
{
    return (bucket->top == -1);
}

int IMPLEMENT(Bucket_full)(const Bucket *bucket)
{
    return (Bucket_size(bucket) == BUCKET_SIZE);

}
