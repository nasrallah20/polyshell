/*-------------------------------------------------------------------------*
 | Copyright (C) 2018 Département Informatique de PolyTech Tours.          |
 |                                                                         |
 | This file is part of PolyShell, yet another shell.                      |
 |                                                                         |
 | PolyShell is free software; you can redistribute it and/or modify       |
 | it under the terms of the GNU General Public License as published by    |
 | the Free Software Foundation; either version 3 of the License,          |
 | or (at your option) any later version.                                  |
 |                                                                         |
 | PolyShell is distributed in the hope that it will be useful,            |
 | but WITHOUT ANY WARRANTY; without even the implied warranty of          |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            |
 | GNU General Public License for more details.                            |
 |                                                                         |
 | You should have received a copy of the GNU General Public License       |
 | along with this program. If not, see <http://www.gnu.org/licenses/>.    |
 *-------------------------------------------------------------------------*/

#include "misc/fifo.h"
#include "misc/string.h"
#include <stdlib.h>
// #########################################################################
// #########################################################################
// #########################################################################

Fifo* IMPLEMENT(Fifo_new)(unsigned int capacity, FifoMode mode)
{
	Fifo *fifo = malloc(sizeof(Fifo));
	if(fifo){
		if(Fifo_init(fifo, capacity, mode)){
			free(fifo);
			return NULL;
		}
	}
    return fifo;
}

int IMPLEMENT(Fifo_init)(Fifo *fifo, unsigned int capacity, FifoMode mode)
{
	if(capacity > 0){
        fifo->mode = mode;
        fifo->head = fifo->tail = 0;
        fifo->capacity = capacity + 1;
        fifo->storage = (char **) malloc(fifo->capacity * sizeof(char*));
        if(fifo->storage == NULL){ return 1;}
        return 0;
    }
    return 1;
}

void IMPLEMENT(Fifo_delete)(Fifo *fifo)
{
	if(fifo){
		Fifo_finalize(fifo);
		free(fifo);
	}
}

void IMPLEMENT(Fifo_finalize)(Fifo *fifo)
{
	Fifo_clear(fifo);
	free(fifo->storage);
}

void IMPLEMENT(Fifo_clear)(Fifo *fifo)
{
	while(!Fifo_empty(fifo)){
		Fifo_pop(fifo);
	}
	fifo->head = fifo->tail = 0;
	//*/provided_Fifo_clear(fifo);
}

int IMPLEMENT(Fifo_push)(Fifo *fifo, const char *str)
{
	if(Fifo_full(fifo)){ return 1; }
	char * tmp = fifo->mode == COMPOSE ? duplicateString(str) : (char *)str;
	if(tmp == NULL) { return 1; }
	fifo->storage[fifo->tail] = tmp;
	fifo->tail = (fifo->tail + 1) % fifo->capacity;
	return 0;
}

const char* IMPLEMENT(Fifo_front)(const Fifo *fifo)
{
    return fifo->storage[fifo->head];
}

void IMPLEMENT(Fifo_pop)(Fifo *fifo)
{
	if(fifo->mode == COMPOSE){ free(fifo->storage[fifo->head]); }
    if(fifo->head + 1 == fifo->capacity){fifo->head = 0;}
	else{++fifo->head;}
}

int IMPLEMENT(Fifo_full)(const Fifo *fifo)
{
    return fifo->head == (fifo->tail + 1) % fifo->capacity;
}

int IMPLEMENT(Fifo_empty)(const Fifo *fifo)
{
    return fifo->head == fifo->tail;
}