/*-------------------------------------------------------------------------*
 | Copyright (C) 2018 Département Informatique de PolyTech Tours.          |
 |                                                                         |
 | This file is part of PolyShell, yet another shell.                      |
 |                                                                         |
 | PolyShell is free software; you can redistribute it and/or modify       |
 | it under the terms of the GNU General Public License as published by    |
 | the Free Software Foundation; either version 3 of the License,          |
 | or (at your option) any later version.                                  |
 |                                                                         |
 | PolyShell is distributed in the hope that it will be useful,            |
 | but WITHOUT ANY WARRANTY; without even the implied warranty of          |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            |
 | GNU General Public License for more details.                            |
 |                                                                         |
 | You should have received a copy of the GNU General Public License       |
 | along with this program. If not, see <http://www.gnu.org/licenses/>.    |
 *-------------------------------------------------------------------------*/

#include "misc/string.h"

// #########################################################################
// #########################################################################
// #########################################################################

char IMPLEMENT(toLowerCase)(char c)
{
	if(c >= 'A' && c <= 'Z'){  c = c - 'A' + 'a';  }
    return c;
}

char IMPLEMENT(toUpperCase)(char c)
{
	if(c >= 'a' && c <= 'z'){  c = c - 'a' + 'A';  }
    return c;
}

size_t IMPLEMENT(stringLength)(const char *str)
{
	int i = 0;
	while (str[i]!='\0'){ ++i; }
	return i;
}

char* IMPLEMENT(duplicateString)(const char *str)
{
	size_t strLength = stringLength(str);
	size_t size = strLength + 1;
	char * res = malloc(size * sizeof(char));
	if(res != NULL){
		copyStringWithLength(res,str,size);
	}
    return res;
}

const char* IMPLEMENT(findFirst)(const char *str, const char *separators)
{
	unsigned int taille_str = stringLength(str), taille_separators = stringLength(separators);
	for(unsigned int i = 0; i < taille_str; ++i){
		for(unsigned int j = 0; j < taille_separators; ++j){
			if(separators[j] == str[i]) { return (str + i); }
		}
	}
    return NULL;
}

char* IMPLEMENT(findLast)(char *str, char c)
{
	unsigned int taille_str = stringLength(str), i;
	char* adr = NULL;
	for(i = 0; i <= taille_str; ++i){
		if(c == str[i]) { adr = (str + i); }
	}
    return adr;
}

int IMPLEMENT(stringCompare)(const char *str1, const char *str2)
{
	int i = 0;
	while(str1[i] != '\0' && str2[i] != '\0'){
		if(str1[i] != str2[i]){
			if(str1[i] > str2[i]) { return 1; }
			else if(str1[i] < str2[i]) { return -1; }
		}
		++i;
	}
    return (stringLength(str1) - stringLength(str2));
}

const char* IMPLEMENT(indexOfString)(const char *foin, const char *aiguille, int csensitive)
{
	unsigned int taille_foin = stringLength(foin), taille_aiguille = stringLength(aiguille);
	const char* adr = NULL;unsigned int i = 0, j = 0;
	while(i != taille_aiguille && j < taille_foin){
		if(csensitive == 0) { 
			if(toLowerCase(aiguille[i]) == toLowerCase(foin[j]) && adr == NULL){ 
				adr = (foin + j); 
				++i;
			} 
			else if(toLowerCase(aiguille[i]) == toLowerCase(foin[j]) && adr != NULL){ 
				++i;
			}
			else if (toLowerCase(aiguille[i]) != toLowerCase(foin[j])){ 
				adr = NULL; 
				i = 0;
			}
		} 
		else {
			if(aiguille[i] == foin[j] && adr == NULL){ 
				adr = (foin + j); 
				++i;
			}
			else if(aiguille[i] == foin[j] && adr != NULL){ 
				++i;
			}
			else if (aiguille[i] != foin[j]){ 
				adr = NULL;
				i = 0;
			}
		}
	++j;
	}
    return adr;
}

char* IMPLEMENT(concatenateStrings)(const char *str1, const char *str2, int minDestSize)
{
	// Verification des parametres
	if(str1 == NULL || str2 == NULL){
		exit(1); // coucou
	}
	int resLength = (int) stringLength(str1) + (int) stringLength(str2) + 1;
	char * res;
	if(resLength <= minDestSize){
		res = (char *) malloc((size_t) minDestSize * sizeof(char));
	}
	else{
		res = (char *) malloc((size_t) resLength * sizeof(char));
	}
	if(res != NULL){
		copyStringWithLength(res,str1,resLength);
		copyStringWithLength((char *) startWith(res,str1),str2,resLength);
	}
    return res;
}

void IMPLEMENT(copyStringWithLength)(char *dest, const char * src, size_t destSize) {
    if(        dest == NULL
        ||      src == NULL
        || destSize ==  0   ) { //printf("error: copyStringWithLength: invalid arguments: abort.\n");
                                exit(1); }
    size_t i = 0, limit = destSize - 1;
    while( src[i]
        && i < limit )
    {
        dest[i] = src[i];
        ++i;
    }
    dest[i] = '\0';
}

char* IMPLEMENT(mkReverse)(char *str, int lengthHint)
{
	if(str == NULL){ 
		exit(1); 
	}
		
	size_t taille_str;
	
	if(lengthHint < 0){ 
		taille_str = stringLength(str); 
	}
	else { 
		taille_str = lengthHint; 
	}
	
	char *tmp; 
	tmp = (char *) malloc( taille_str + 1 * sizeof(char));
	size_t i = 0;
	while(i != taille_str){
		tmp[i] = str[taille_str - 1 - i];
		++i;
	}
    tmp[i] = '\0';
	
	copyStringWithLength(str,tmp,taille_str + 1);
	free(tmp);
    return str;
}

const char* IMPLEMENT(startWith)(const char *str, const char *prefix)
{
	unsigned int taille_prefix = stringLength(prefix), i = 0;
	while(i < taille_prefix){
		if(str[i] != prefix[i]){ return NULL; }
		++i;
	}
    return (str + i);
}

int IMPLEMENT(belongs)(char c, const char *str)
{
	unsigned int taille_str = stringLength(str);
	for(unsigned int i = 0; i < taille_str; ++i){
		if(c == str[i]) { return 1; }
	}
    return 0;
}

char* IMPLEMENT(subString)(const char *start, size_t length)
{
	if(start == NULL){
		exit(1);
	}
	else{
		char * res = malloc( length + 1 * sizeof(char));
        copyStringWithLength(res,start,length + 1);
		return res;
	}
}

void IMPLEMENT(mkCommon)(char *result, const char *str)
{
	if(result == NULL || str == NULL) { exit(1); }
	unsigned int resultLength = stringLength(result);
	int stop = 0; size_t i = 0;
	while(stop == 0 && i < resultLength){
		if(result[i] != str[i]){
			result[i] = '\0';
			--stop;
		}
		++i;
	}
}

int IMPLEMENT(isNotEmpty)(const char *str)
{
	unsigned int taille_str = stringLength(str);
    for(unsigned int i = 0; i < taille_str; ++i){
		if(str[i] != ' ') { return 1; }
	}
    return 0;
}

char* IMPLEMENT(getProtString)(const char *str, char c)
{
	if(str == NULL){ exit(1); }
	
	size_t taille_str = stringLength(str);;
	char *tmp;
	
	if(!belongs(c,str)){ 
		tmp = duplicateString((char *) str); 
		return tmp;
	}  
	else{
		size_t count = 0;
		for(unsigned int i = 0; i < taille_str; ++i){
			if(str[i] == c){ ++count; }
		}
		tmp = (char *) calloc( (taille_str + count + 1), sizeof(char));
		size_t i = 0;
		size_t j = 0;
		while(j <= taille_str){
			tmp[i] = str[j];
			if(tmp[i] == c){
				++i;
				tmp[i] = c;
			}
			++i;++j;
		}
	}
	return tmp;
    //return provided_getProtString(str, c);
}

char* IMPLEMENT(getRealString)(const char *str, char c, char **firstNotEscaped)
{
	if(str == NULL){ exit(1); }
	
	size_t taille_str = stringLength(str);
	char *tmp;
	*firstNotEscaped = NULL; 
	
	if(!belongs(c,str)){ 
		tmp = duplicateString((char *) str); 
		return tmp;
	}  
	else{
		size_t count = 0;
		for(unsigned int i = 0; i < taille_str; ++i){
			if(str[i] == c && str[i + 1 ] == c ){ ++count; }
		}
		tmp = (char *) malloc( (taille_str - count + 1) * sizeof(char));
		size_t taille_fin = taille_str - count;
		size_t i = 0;
		size_t j = 0;
		while(j <= taille_str){
			tmp[i] = str[j];
			if(str[j] == c && j != taille_fin && str[j + 1] != c && *firstNotEscaped == NULL){
				*firstNotEscaped = &tmp[i];
			}
			else if(str[j] == c && str[j + 1] == c){
				++j;
			}
			++i;
			++j;
		}
		//tmp[i] = '\0';
	}
	return tmp;
    //return provided_getRealString(str, c, firstNotEscaped);
}

Tokenizer* IMPLEMENT(Tokenizer_new)(const char *str, const char *separators)
{
    Tokenizer *token = malloc(sizeof(Tokenizer));
	if(token){
		if(Tokenizer_init(token, str, separators)){
			free(token);
			return NULL;
		}
		return token;
	}
	free(token);
	return NULL;
    //*/return provided_Tokenizer_new(str, separators);
}

int IMPLEMENT(Tokenizer_init)(Tokenizer *tokenizer, const char *str, const char *separators)
{
	if(str != NULL && separators != NULL && tokenizer != NULL){
		tokenizer->str = "";
		tokenizer->next = str;
		tokenizer->separators = separators;
		if(tokenizer->separators && tokenizer->next){
			Tokenizer_next(tokenizer);
			return 0;
		}
	}
	return 1;
    //*/return provided_Tokenizer_init(tokenizer, str, separators);
}

void IMPLEMENT(Tokenizer_delete)(Tokenizer *tokenizer)
{
   if(tokenizer){
		Tokenizer_finalize(tokenizer);
		free(tokenizer);
	}
    //*/provided_Tokenizer_delete(tokenizer);
}

void IMPLEMENT(Tokenizer_finalize)(Tokenizer *tokenizer)
{
	tokenizer->str = NULL;
	tokenizer->separators = NULL;
	tokenizer->next = NULL;
    //*/provided_Tokenizer_finalize(tokenizer);
}

int IMPLEMENT(Tokenizer_hasNext)(const Tokenizer *tokenizer)
{
	return (tokenizer->str != NULL);
    //*/return provided_Tokenizer_hasNext(tokenizer);
}

char* IMPLEMENT(Tokenizer_get)(const Tokenizer *tokenizer)
{
	size_t i = 0;
	size_t taillestr = stringLength(tokenizer->str);
	if(stringCompare(tokenizer->separators, "") == 0){
		char * res = duplicateString(tokenizer->str);
		return res;
	}
	else{
		while(i < taillestr && !belongs(tokenizer->str[i], tokenizer->separators)){
			++i;
		}
		char * res = malloc(sizeof(char) * (i + 1));
		copyStringWithLength(res, tokenizer->str, i + 1);
		//res[i + 1] = '\0';
		return res;
	}
	return NULL;
    //*/return provided_Tokenizer_get(tokenizer);
}

void IMPLEMENT(Tokenizer_next)(Tokenizer *tokenizer)
{
	if(tokenizer->next == NULL || stringCompare(tokenizer->next, "") == 0){
		tokenizer->str = NULL;
		tokenizer->next = NULL;
	}
	else if(stringCompare(tokenizer->separators, "") == 0){
		tokenizer->str = tokenizer->next;
		tokenizer->next = NULL;
	}
	else{
		while(belongs(tokenizer->next[0], tokenizer->separators)){
			++tokenizer->next;
		}
		tokenizer->str = tokenizer->next;
		if(stringCompare(tokenizer->str, "") == 0){
			tokenizer->str = NULL;
		}
		else{
			tokenizer->next = findFirst(tokenizer->next, tokenizer->separators);
		}
	}
    //*/provided_Tokenizer_next(tokenizer);
}
