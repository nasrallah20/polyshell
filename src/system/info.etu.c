/*-------------------------------------------------------------------------*
 | Copyright (C) 2018 Département Informatique de PolyTech Tours.          |
 |                                                                         |
 | This file is part of PolyShell, yet another shell.                      |
 |                                                                         |
 | PolyShell is free software; you can redistribute it and/or modify       |
 | it under the terms of the GNU General Public License as published by    |
 | the Free Software Foundation; either version 3 of the License,          |
 | or (at your option) any later version.                                  |
 |                                                                         |
 | PolyShell is distributed in the hope that it will be useful,            |
 | but WITHOUT ANY WARRANTY; without even the implied warranty of          |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            |
 | GNU General Public License for more details.                            |
 |                                                                         |
 | You should have received a copy of the GNU General Public License       |
 | along with this program. If not, see <http://www.gnu.org/licenses/>.    |
 *-------------------------------------------------------------------------*/

#include "system/info.h"
#include "misc/string.h"
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <pwd.h>
#include <stdio.h>
// #########################################################################
// #########################################################################
// #########################################################################

int IMPLEMENT(isRoot)(void)
{
	return getuid() == 0;
    //*/return provided_isRoot();
}

const char* IMPLEMENT(getPath)(void)
{
	return getenv("PATH");
    //*/return provided_getPath();
}

const char* IMPLEMENT(hostname)(void)
{
	static char hostbuffer[256] = "";
	if(hostbuffer[0] == '\0'){
		if(gethostname(hostbuffer, 256) == 1){
			copyStringWithLength(hostbuffer, "Unknown", 256);
		}
	}
	return hostbuffer;
    //*/return provided_hostname();
}

int IMPLEMENT(userInformation)(const char **un, const char **hd, const char **sh)
{
	static char unbuffer[256] = "", hdbuffer[256] = "", shbuffer[256] = "";
	uid_t uid = geteuid();
	struct passwd * pw = getpwuid(uid);
	if(!pw){
		return 1;
	}
			
	if(un != NULL){
		if(unbuffer[0] == '\0'){
			copyStringWithLength(unbuffer, pw->pw_name, 256);
		}
		*un = unbuffer;
	}
	
	if(hd != NULL){
		if(hdbuffer[0] == '\0'){
			copyStringWithLength(hdbuffer, pw->pw_dir, 256);
		}
		*hd = hdbuffer;
	}
	
	if(sh != NULL){
		if(shbuffer[0] == '\0'){
			copyStringWithLength(shbuffer, pw->pw_shell, 256);
		}
		*sh = shbuffer;
	}
	
	return 0;
    //*/return provided_userInformation(un, hd, sh);
}

const char* IMPLEMENT(getCurrentDirectory)(int compact)
{
	static char curDirbuffer[256] = "";
	if(getcwd(curDirbuffer, 256) == NULL){
		return NULL;
	}
	if(compact == 1){
		const char * pb = startWith(curDirbuffer, getenv("HOME"));
		if(pb != NULL){
            char *poubelle = concatenateStrings("~", pb, -1);
			copyStringWithLength(curDirbuffer, poubelle, 256);
            free(poubelle);
		}
	}
	return curDirbuffer;
    //*/return provided_getCurrentDirectory(compact);
}
