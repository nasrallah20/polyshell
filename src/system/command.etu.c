/*-------------------------------------------------------------------------*
 | Copyright (C) 2018 Département Informatique de PolyTech Tours.          |
 |                                                                         |
 | This file is part of PolyShell, yet another shell.                      |
 |                                                                         |
 | PolyShell is free software; you can redistribute it and/or modify       |
 | it under the terms of the GNU General Public License as published by    |
 | the Free Software Foundation; either version 3 of the License,          |
 | or (at your option) any later version.                                  |
 |                                                                         |
 | PolyShell is distributed in the hope that it will be useful,            |
 | but WITHOUT ANY WARRANTY; without even the implied warranty of          |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            |
 | GNU General Public License for more details.                            |
 |                                                                         |
 | You should have received a copy of the GNU General Public License       |
 | along with this program. If not, see <http://www.gnu.org/licenses/>.    |
 *-------------------------------------------------------------------------*/

#include "system/command.h"
#include "interactive/autocomplete.h"
#include "misc/config.h"
#include "misc/string.h"
#include "misc/fifo.h"
#include "system/info.h"
#include "misc/filesystem.h"
#include "misc/regex.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

// #########################################################################
// #########################################################################
// #########################################################################

Command* IMPLEMENT(Command_new)(const char *base)
{
    Command *commande= malloc(sizeof(Command));
	if(commande){
		if(Command_init(commande, base)){
			free(commande);
			return NULL;
		}
		if(Command_addLivingCommand(commande)){
			Command_delete(commande); return NULL;
		}
	}
    return commande;
    //return provided_Command_new(base);
}

int IMPLEMENT(Command_init)(Command *cmd, const char *base)
{
	cmd->status = 1;
	cmd->base = duplicateString(base);
	if(cmd->base == NULL){ 
		return 1; 
	}
	cmd->capacityOption = 8;
	cmd->nbOptions = 1;
	cmd->options = malloc(sizeof(char *) * cmd->capacityOption);
	if(cmd->options == NULL){ 
		free(cmd->base); 
		free(cmd->options); 
		return 1; 
	}
	cmd->options[0] = duplicateString(base);
	if(cmd->options[0] == NULL){ 
		free(cmd->base); 
		free(cmd->options); 
		return 1; 
	}
	cmd->next = cmd->prev = NULL;
	for(int i = 0; i < 3; ++i){
		cmd->redirections[i] = NULL;
		cmd->redirectionTypes[i] = UNDEFINED;
	}
	if(stringCompare(base, "su") == 0){
		Command_addOption(cmd, "-s", 0);
		Command_addOption(cmd, Configuration.exec, 0);
		if(cmd->status == 0){ 
			Command_finalize(cmd); 
			return 1; 
		}
	}
	return 0;
    //return provided_Command_init(cmd, base);
}

void IMPLEMENT(Command_delete)(Command *cmd)
{
    if(cmd){
		Command_finalize(cmd);
		free(cmd);
	}
    //provided_Command_delete(cmd);
}

void IMPLEMENT(Command_finalize)(Command *cmd)
{
	free(cmd->base);
	for(unsigned int i= 0; i < cmd->nbOptions; ++i){
        free(cmd->options[i]);
	}
	for(int i= 0; i < 3; ++i){
        free(cmd->redirections[i]);
		cmd->redirectionTypes[i] = UNDEFINED;
	}
	free(cmd->options);
	cmd->next = NULL;
	cmd->prev = NULL;
	cmd->capacityOption = -1;
	cmd->nbOptions = -1;
	cmd->status = -1;
    //*/provided_Command_finalize(cmd);
}

Command* IMPLEMENT(Command_redirect)(Command *cmd, int fd, const char *filename)
{
	if(cmd && cmd->status){
		if(fd < 0 || fd > 2){
			cmd->status = 0;
			return cmd;
		}
		char *tmp = duplicateString(filename);
		if(tmp == NULL){
			cmd->status = 0;
			return cmd;
		}
		if(cmd->redirections[fd]){
			free(cmd->redirections[fd]);
		}
		cmd->redirections[fd] = tmp;
		cmd->redirectionTypes[fd] = NORMAL;
	}
	return cmd;
    //return provided_Command_redirect(cmd, fd, filename);
}

Command* IMPLEMENT(Command_appendRedirect)(Command *cmd, int fd, const char *filename)
{
	if(cmd && cmd->status){
		if(fd < 0 || fd > 2){
			cmd->status = 0;
			return cmd;
		}
		char *tmp = duplicateString(filename);
		if(tmp == NULL){
			cmd->status = 0;
			return cmd;
		}
		if(cmd->redirections[fd]){
			free(cmd->redirections[fd]);
		}
		cmd->redirections[fd] = tmp;
		cmd->redirectionTypes[fd] = APPEND;
	}
	return cmd;
    //return provided_Command_appendRedirect(cmd, fd, filename);
}

Command* IMPLEMENT(Command_mergeOutputs)(Command *cmd)
{
	if(cmd && cmd->status){
		if(cmd->redirections[2]){
			free(cmd->redirections[2]);
		}
		cmd->redirections[2] = NULL;
		cmd->redirectionTypes[2] = FUSION;
	}
	return cmd;
    //return provided_Command_mergeOutputs(cmd);
}

Command* IMPLEMENT(Command_pipe)(Command *c1, Command *c2)
{
	if(c1 && c2){
		if(c1->status != 0 && c2->status != 0){
			if(c1->next == NULL && c2->prev == NULL){
				 c1->next = c2;
				 c2->prev = c1;
				 return c2;
			}
		}
		c1->status = 0;
		c2->status = 0;
		return c2;
	}
	return NULL;
    //*/return provided_Command_pipe(c1, c2);
}

void Commmand_getOptionFolderMatch(const char * currentDir, Fifo * fifoDossier, Pattern * pattern){
	FolderIterator * folderDir = FolderIterator_new(currentDir, 1);
	while(FolderIterator_hasNext(folderDir)){
		char * poubelle = (char *) FolderIterator_get(folderDir);
		if(FolderIterator_isDir(folderDir)){
			if(Pattern_match(pattern, poubelle)){
				char * poubelle2 = concatenateStrings(poubelle, "/", 0);
				Fifo_push(fifoDossier, poubelle2);
				free(poubelle2);
			}
		}
		FolderIterator_next(folderDir);
	}
	FolderIterator_delete(folderDir);
}

int Commmand_getOptionFileMatch(const char * currentDir, Fifo * fifoDossier, Pattern * pattern){
	int cpt = 0;
	FolderIterator * folderDir = FolderIterator_new(currentDir, 1);
	while(FolderIterator_hasNext(folderDir)){
		char * poubelle = (char *) FolderIterator_get(folderDir);
		if(Pattern_match(pattern, poubelle)){
			Fifo_push(fifoDossier, poubelle);
			++cpt;
		}
		FolderIterator_next(folderDir);
	}
	FolderIterator_delete(folderDir);
	return cpt;
}

int Commmand_getOptionFileMatchHard(const char * currentDir, Fifo * fifoDossier, Fifo * fifoption, Pattern * pattern){
	int compteur = 0;
	while(!Fifo_empty(fifoDossier)){
		const char * subdir = Fifo_front(fifoDossier);
		char * tmp = concatenateStrings(currentDir, "/", -1);
		char * lastdir = concatenateStrings(tmp, subdir, -1);
		free(tmp);
		FolderIterator * folderDir = FolderIterator_new(lastdir, 1);
		while(FolderIterator_hasNext(folderDir)){
			char * poubelle = (char *) FolderIterator_get(folderDir);
			if(!FolderIterator_isDir(folderDir)){
				if(Pattern_match(pattern, poubelle)){
					char * trol = concatenateStrings(subdir, poubelle, -1);
					Fifo_push(fifoption, trol);
					free(trol);
					++compteur;
				}
			}
			FolderIterator_next(folderDir);
		}
		FolderIterator_delete(folderDir);
		Fifo_pop(fifoDossier);
		free(lastdir);
	}
	return compteur;
}


void InitialisePattern(Pattern * pregDossier, const char * option, char * pattern){
	int lenoption = stringLength(option);
	int j = 1;
	pattern[0] = '^';
	for(int i = 0; i < lenoption; ++i){
		if(option[i] == '*'){
			pattern[j] = '.';
			++j;
		}
		else if(option[i] == '.'){
			pattern[j] = '\\';
			++j;
		}
		pattern[j] = option[i];
		++j;
	}
	pattern[j] = '$';
	pattern[j + 1] = '\0';
	Pattern_init(pregDossier, pattern);
}

Command* IMPLEMENT(Command_addOption)(Command *cmd, const char *option, int expend)
{
	/*if(cmd && cmd->status != 0){
	    if(expend == 0 || (option == NULL && expend == 1) || (!belongs('*',option) && expend == 1)){
			if(cmd->capacityOption == cmd->nbOptions + 1){
				cmd->capacityOption = cmd->capacityOption * 2;
				cmd->options = realloc(cmd->options, cmd->capacityOption * sizeof(char *));
				if(option)
					cmd->options[cmd->nbOptions] = duplicateString(option);
				else
					cmd->options[cmd->nbOptions] = NULL;
				++cmd->nbOptions;
				return cmd;
			}
			else{
				if(option)
					cmd->options[cmd->nbOptions] = duplicateString(option);
				else
					cmd->options[cmd->nbOptions] = NULL;
				++cmd->nbOptions;
				return cmd;
			}
		}
		else{
			const char * currentDir = getCurrentDirectory(0);
			char * lastprefix = duplicateString(option);
			char * lastDir;
			char * pos = NULL;
			FifoMode mode = COMPOSE;
			int lenoption = stringLength(option);
			
			if(!belongs('/', option)){
				lastDir = duplicateString(currentDir);
				
				Pattern * pregsimple = malloc(sizeof(Pattern));
				
				int nbchartoadd = 3;
				for(int i = 0; i < lenoption; ++i){
					if(option[i] == '*' || option[i] == '.'){
						++nbchartoadd;
					}
				}
				char * tobesup = malloc(sizeof(char) * lenoption + nbchartoadd );
				InitialisePattern(pregsimple, lastprefix, tobesup);
				
				Fifo * Fifosimple = Fifo_new(1024, mode);
				
				int nboptionstoaddsimple = Commmand_getOptionFileMatch(lastDir, Fifosimple, pregsimple);
				
				Pattern_delete(pregsimple);
				free(tobesup);
				
				while(cmd->capacityOption <= cmd->nbOptions + nboptionstoaddsimple){
					cmd->capacityOption = cmd->capacityOption * 2;
				}
				cmd->options = realloc(cmd->options, cmd->capacityOption * sizeof(char *));
				while(!Fifo_empty(Fifosimple)){
					const char * toadd = Fifo_front(Fifosimple);
					cmd->options[cmd->nbOptions] = duplicateString(toadd);
					++cmd->nbOptions;
					Fifo_pop(Fifosimple);
				}
				Fifo_delete(Fifosimple);
				free(lastDir);
				free(lastprefix);
				return cmd;
			}
			else{
				char * prefixtemp = prependHomeDir(lastprefix);
				char * prefixtempbis = getRealString(prefixtemp, '/' , &pos);
				char * lastprefixtmp = findLast((char *)prefixtempbis, '/') + 1;
				lastDir = subString(prefixtempbis, stringLength(prefixtempbis) - stringLength(lastprefixtmp) - 1);
				free(lastprefix);
				lastprefix = duplicateString(lastprefixtmp);
				free(prefixtempbis);
				
				Pattern * pregDossier = malloc(sizeof(Pattern));
				char * tobesup = malloc(sizeof(char) * lenoption * 2);
				InitialisePattern(pregDossier, lastDir, tobesup);
				
				Fifo * FifoDir= Fifo_new(1024, mode);
				
				
				Commmand_getOptionFolderMatch(currentDir, FifoDir, pregDossier);
				
				Pattern_delete(pregDossier);
				free(tobesup);
				
				Pattern * pregFile = malloc(sizeof(Pattern));
				tobesup = malloc(sizeof(char) * lenoption * 2);
				InitialisePattern(pregFile, lastprefix, tobesup);
	
				Fifo * Fifooptions = Fifo_new(1024, mode);
				
				
				int nboptionstoadd = Commmand_getOptionFileMatchHard(currentDir, FifoDir, Fifooptions, pregFile);
				
				Fifo_delete(FifoDir);
				Pattern_delete(pregFile);
				free(tobesup);
				
				while(cmd->capacityOption <= cmd->nbOptions + nboptionstoadd){
					cmd->capacityOption = cmd->capacityOption * 2;
				}
				cmd->options = realloc(cmd->options, cmd->capacityOption * sizeof(char *));
				while(!Fifo_empty(Fifooptions)){
					const char * toadd = Fifo_front(Fifooptions);
					cmd->options[cmd->nbOptions] = duplicateString(toadd);
					++cmd->nbOptions;
					Fifo_pop(Fifooptions);
				}
				Fifo_delete(Fifooptions);
				free(lastDir);
				free(lastprefix);
				return cmd;				
			}
		}
	}
	return cmd;
    //*/return provided_Command_addOption(cmd, option, expend);
}
size_t IMPLEMENT(Command_getNbMember)(const Command *cmd)
{
	Command *tmp = cmd->next;
	size_t count = 0;
	if(cmd->status == 1){
		++count;
	}
	while(tmp != NULL && tmp->status == 1){
		++count;
		tmp = tmp->next;
	}
	tmp = cmd->prev;
	while(tmp != NULL && tmp->status == 1){
		++count;
		tmp = tmp->prev;
	}
	return count;
    //return provided_Command_getNbMember(cmd);
}

int IMPLEMENT(Command_execute)(Command *cmd)
{
	// Retour à la commande de départ
	while(cmd->prev != NULL){
		cmd = cmd->prev;
	}
	
	if(stringCompare(cmd->base, "exit") == 0){
		exit(0);
	}
	if(stringCompare(cmd->base, "cd") == 0){
		if(cmd->nbOptions > 2){
			exit(EXIT_FAILURE);
		} 
		else{
			if(cmd->nbOptions == 1){
				Command_addOption(cmd, getenv("HOME"), 0);
			}
			if(chdir(prependHomeDir(cmd->options[1])) == -1){
				perror("cd error ! ");
				return 1;
			} 
			else{
				return 0;
			}
		}
	}
	
	//int idPere = getpid();
	
	int nbCmd = Command_getNbMember(cmd);
	int TabEnfant[nbCmd];
	
	int TabEnfantPid[nbCmd + 1];
	char ** TabEnfantName = malloc(sizeof(char *) * nbCmd + 1);
	
	char * sh = "shell";
	
	TabEnfantPid[0] = getpid();
	TabEnfantName[0] = sh;
	
	int TabEnfantDesc[nbCmd - 1][2];
	for(int i = 0; i < nbCmd; ++i){
		if(pipe(TabEnfantDesc[i])){
			return 1;
		}
	}
	
	
	
	//Boucle récursive pour gêrer toutes les commandes
	int act = 0;
	while(cmd != NULL){
		//Ajout de l'option NULL pour chaque commande pour le execvp
		Command_addOption(cmd, NULL, 0);
		TabEnfant[act] = fork();
		if(TabEnfant[act] == -1)
		{
			
		}
		//Verification proc en cours
		else if(TabEnfant[act] != 0){
			TabEnfantPid[act + 1] = TabEnfant[act];
			TabEnfantName[act + 1] = cmd->base;
			if(cmd->prev != NULL){
				close(TabEnfantDesc[act - 1][0]);
			}
			if(cmd->next != NULL){
				close(TabEnfantDesc[act][1]);
			}
			wait(NULL);
		}
		else {
			// Ici proc fils
			//Gestion de la redirection d'entrée
			if(cmd->redirections[0] != NULL){
				// On verifie que l'entree est bonne
				int EnfEntree = open(cmd->redirections[0], O_RDWR, 0666);
				if(EnfEntree == -1){
					perror("Entrée éronnée !");
					return 1;
				}
				//on applique cette entrée au proc en cours
				dup2(EnfEntree, STDIN_FILENO);
				close(EnfEntree);
			}
			
			
			//Gestion de la redirection de sortie
			if(cmd->redirections[1] != NULL){
				//On s'adapte au type de la sortie
				int typeSortie = O_RDWR | O_CREAT;
				
				if(cmd->redirectionTypes[1] == NORMAL){
					typeSortie = typeSortie | O_TRUNC;
				}
				else if(cmd->redirectionTypes[1] == APPEND){
					typeSortie = typeSortie | O_APPEND;
				}
				
				// On verifie que la sortie est bonne
				int EnfSortie = open(cmd->redirections[1], typeSortie, 0666);
				if(EnfSortie == -1){
					perror("Sortie éronnée !");
					return 1;
				}
				//on applique cette sortie au proc en cours
				dup2(EnfSortie, STDOUT_FILENO);
				close(EnfSortie);
			}
			
			
			//Gestion de la redirection de sortie d'erreur
			if(cmd->redirections[2] != NULL){
				//On s'adapte au type de la sortie d'erreurç
				int typeSortieErr = O_RDWR | O_CREAT;
				
				if(cmd->redirectionTypes[2] == NORMAL){
					typeSortieErr = typeSortieErr | O_TRUNC;
				}
				else if(cmd->redirectionTypes[2] == APPEND){
					typeSortieErr = typeSortieErr | O_APPEND;
				}
				
				// On verifie que la sortie est bonne
				int EnfSortieErr = open(cmd->redirections[2], typeSortieErr, 0666);
				if(EnfSortieErr == -1){
					perror("Sortie d'erreur éronnée !");
					return 1;
				}
				//on applique cette sortie erreur au proc en cours
				dup2(EnfSortieErr, STDERR_FILENO);
				close(EnfSortieErr);
			}
			else if(cmd->redirectionTypes[2] == FUSION){
				int EnfSortieErr = 1;
				dup2(EnfSortieErr, STDERR_FILENO);
				close(EnfSortieErr);
			}
			
			
			
			//On connecte l'entrée ou la sortie des commandes pour chaque proc si besoin
			if(cmd->prev != NULL){
				dup2(TabEnfantDesc[act - 1][0], STDIN_FILENO);
				close(TabEnfantDesc[act - 1][0]);
			}
			if(cmd->next != NULL){
				dup2(TabEnfantDesc[act][1], STDOUT_FILENO);
				close(TabEnfantDesc[act][1]);
			}
			
			//On execute la commande 
            execvp(cmd->base, cmd->options);
		}
		cmd = cmd->next;
		++act;
	}
	int Errcode = 0;
	Errcode = waitpid(TabEnfantPid[1], &Errcode, WUNTRACED | WCONTINUED);
	return Errcode;
	//*/return provided_Command_execute(cmd);
}
