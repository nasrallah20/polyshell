/*-------------------------------------------------------------------------*
 | Copyright (C) 2018 Département Informatique de PolyTech Tours.          |
 |                                                                         |
 | This file is part of PolyShell, yet another shell.                      |
 |                                                                         |
 | PolyShell is free software; you can redistribute it and/or modify       |
 | it under the terms of the GNU General Public License as published by    |
 | the Free Software Foundation; either version 3 of the License,          |
 | or (at your option) any later version.                                  |
 |                                                                         |
 | PolyShell is distributed in the hope that it will be useful,            |
 | but WITHOUT ANY WARRANTY; without even the implied warranty of          |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            |
 | GNU General Public License for more details.                            |
 |                                                                         |
 | You should have received a copy of the GNU General Public License       |
 | along with this program. If not, see <http://www.gnu.org/licenses/>.    |
 *-------------------------------------------------------------------------*/

#include "interactive/history.h"
#include "misc/string.h"
#include "interactive/autocomplete.h"
#include "misc/filesystem.h"
#include "misc/fifo.h"
#include <stdlib.h>
#include <stdio.h>
// #########################################################################
// #########################################################################
// #########################################################################

History* IMPLEMENT(History_new)(const char *filename, unsigned int length)
{
    History *history= malloc(sizeof(History));
	if(history){
		if(History_init(history, filename,length)){
			free(history);
			history = NULL;
		}
	}
    return history;
    //return provided_History_new(filename, length);
}

int IMPLEMENT(History_init)(History *history, const char *filename, unsigned int length)
{
    if(Fifo_init(&history->storage, length, COMPOSE)){
		return 1;
	}
	history->position = history->storage.tail;
	
	if(filename){
		char * pathFile = prependHomeDir(duplicateString(filename));
		if(pathFile){
			FILE *fichier = fopen(pathFile, "r");
			if(fichier){
				FileIterator *fichierit = FileIterator_new(fichier);
                if(fichierit){
                    while(FileIterator_hasNext(fichierit)){
                        History_add(history, FileIterator_get(fichierit));
                        FileIterator_next(fichierit);
                    }
                    FileIterator_delete(fichierit);
                }
                fclose(fichier);
            }
			free(pathFile);
		}
	}
	return 0;
    //return provided_History_init(history, filename, length);
}

void IMPLEMENT(History_delete)(History *history, const char *filename)
{
   if(history){
		History_finalize(history, filename);
		free(history);
	}
    //provided_History_delete(history, filename);
}

void IMPLEMENT(History_finalize)(History *history, const char *filename)
{
	if(filename){
		char * pathFile = prependHomeDir(duplicateString(filename));
		if(pathFile){
			FILE *fichier = fopen(pathFile, "w+");
			if(fichier){
				while(!Fifo_empty(&history->storage)){
					char * res = getProtString(Fifo_front(&history->storage), '#');
					fprintf(fichier, "%s\n", res);
					Fifo_pop(&history->storage);
					free(res);
				}
            }
            fclose(fichier);
		}
        free(pathFile);
	}
	Fifo_finalize(&history->storage);
    //*/provided_History_finalize(history, filename);
}

void IMPLEMENT(History_clear)(History *history)
{
	Fifo_clear(&history->storage);
	history->position = 0;
    //provided_History_clear(history);
}

void IMPLEMENT(History_add)(History *history, const char *cmd)
{
	if(history != NULL && cmd != NULL && cmd[0] != '\0'){
		if(Fifo_full(&history->storage)){
			Fifo_pop(&history->storage);
		}
		Fifo_push(&history->storage, cmd);
		history->position = history->storage.tail;
	}
    //provided_History_add(history, cmd);
}

const char* IMPLEMENT(History_up)(History *history)
{
	if(history && history->position != history->storage.head){
		const char * res = NULL;
		if(history->position > history->storage.head || history->position <= history->storage.tail){
			if(history->position == 0){
				res = (const char *) history->storage.storage[history->storage.capacity - 1];
				history->position = history->storage.capacity - 1;
			}
			else{
				res = (const char *) history->storage.storage[history->position - 1];
				history->position = history->position - 1;
			}
		}
		return res;
	}
	return NULL;
    //return provided_History_up(history);
}

const char* IMPLEMENT(History_down)(History *history)
{
	if(history && history->position != history->storage.tail){
		const char * res = NULL;
		if(history->position == history->storage.tail - 1){
			history->position = history->storage.tail;
			return "";
		}
		else if(history->position >= history->storage.head || history->position < history->storage.tail){
			if(history->position == history->storage.capacity - 1){
				res = (const char *) history->storage.storage[0];
				history->position = 0;
			}
			else{
				res = (const char *) history->storage.storage[history->position + 1];
				history->position = history->position + 1;
			}
		}
		return res;
	}
	return NULL;
    //return provided_History_down(history);
}
