/*-------------------------------------------------------------------------*
 | Copyright (C) 2018 Département Informatique de PolyTech Tours.          |
 |                                                                         |
 | This file is part of PolyShell, yet another shell.                      |
 |                                                                         |
 | PolyShell is free software; you can redistribute it and/or modify       |
 | it under the terms of the GNU General Public License as published by    |
 | the Free Software Foundation; either version 3 of the License,          |
 | or (at your option) any later version.                                  |
 |                                                                         |
 | PolyShell is distributed in the hope that it will be useful,            |
 | but WITHOUT ANY WARRANTY; without even the implied warranty of          |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            |
 | GNU General Public License for more details.                            |
 |                                                                         |
 | You should have received a copy of the GNU General Public License       |
 | along with this program. If not, see <http://www.gnu.org/licenses/>.    |
 *-------------------------------------------------------------------------*/

#include "misc/regex.h"

// #########################################################################
// #########################################################################
// #########################################################################

Pattern* IMPLEMENT(Pattern_new)(const char *pattern)
{
    Pattern * Regpattern = malloc(sizeof(Pattern));
	if(Regpattern && pattern){
		if(Pattern_init(Regpattern, pattern)){
			regfree(&Regpattern->preg);
			free(Regpattern);
			return NULL;
		}
		return Regpattern;
	}
	free(Regpattern);
	return NULL;
    //*/return provided_Pattern_new(pattern);
}

int IMPLEMENT(Pattern_init)(Pattern *preg, const char *pattern)
{
	return regcomp(&preg->preg, pattern, REG_EXTENDED | REG_NOSUB);
    //*/return provided_Pattern_init(preg, pattern);
}

void IMPLEMENT(Pattern_delete)(Pattern *preg)
{
   if(preg){
		Pattern_finalize(preg);
		free(preg);
	}
    //*/provided_Pattern_delete(preg);
}

void IMPLEMENT(Pattern_finalize)(Pattern *preg)
{
	regfree(&preg->preg);
    //*/provided_Pattern_finalize(preg);
}

int IMPLEMENT(Pattern_match)(const Pattern *preg, const char *str)
{
	if(regexec(&preg->preg, str, stringLength(str), NULL, 0) == 0){
		return 1;
	} 
	return 0;
    //*/return provided_Pattern_match(preg, str);
}
