/*-------------------------------------------------------------------------*
 | Copyright (C) 2018 Département Informatique de PolyTech Tours.          |
 |                                                                         |
 | This file is part of PolyShell, yet another shell.                      |
 |                                                                         |
 | PolyShell is free software; you can redistribute it and/or modify       |
 | it under the terms of the GNU General Public License as published by    |
 | the Free Software Foundation; either version 3 of the License,          |
 | or (at your option) any later version.                                  |
 |                                                                         |
 | PolyShell is distributed in the hope that it will be useful,            |
 | but WITHOUT ANY WARRANTY; without even the implied warranty of          |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            |
 | GNU General Public License for more details.                            |
 |                                                                         |
 | You should have received a copy of the GNU General Public License       |
 | along with this program. If not, see <http://www.gnu.org/licenses/>.    |
 *-------------------------------------------------------------------------*/

#include "misc/filesystem.h"
#include "misc/string.h"
#include "interactive/autocomplete.h"
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
// #########################################################################
// #########################################################################
// #########################################################################

FolderIterator* IMPLEMENT(FolderIterator_new)(const char *path, int skipSpecials)
{
	FolderIterator *fIterator= malloc(sizeof(FolderIterator));
	if(fIterator){
		if(FolderIterator_init(fIterator, path, skipSpecials)){
			free(fIterator);
			fIterator = NULL;
		}
	}
    return fIterator;
    //return provided_FolderIterator_new(path, skipSpecials);
}

int IMPLEMENT(FolderIterator_init)(FolderIterator *fIterator, const char *path, int skipSpecials)
{
	if(path){
		fIterator->skipSpecials = skipSpecials;
		DIR *dossier = opendir(path);
		if(dossier){
			fIterator->dir = dossier;
			FolderIterator_next(fIterator);
			//if(skipSpecials == 0 && !FolderIterator_hasNext(fIterator)){ return 1; }
			return 0;
		}
	}
	return 1;
    //return provided_FolderIterator_init(fIterator, path, skipSpecials);
}

void IMPLEMENT(FolderIterator_delete)(FolderIterator *fIterator)
{
	if(fIterator){
		FolderIterator_finalize(fIterator);
		free(fIterator);
	}
    //provided_FolderIterator_delete(fIterator);
}

void IMPLEMENT(FolderIterator_finalize)(FolderIterator *fIterator)
{
	closedir(fIterator->dir);
	fIterator->dir = NULL;
	fIterator->ent = NULL;
    //provided_FolderIterator_finalize(fIterator);
}

int IMPLEMENT(FolderIterator_hasNext)(const FolderIterator *fIterator)
{
	return (fIterator->dir != NULL && fIterator->ent != NULL);
	//return provided_FolderIterator_hasNext(fIterator);
}

const char* IMPLEMENT(FolderIterator_get)(const FolderIterator *fIterator)
{
	return (const char *) fIterator->ent->d_name;
    //return provided_FolderIterator_get(fIterator);
}

int IMPLEMENT(FolderIterator_isDir)(const FolderIterator *fIterator)
{
	if(fIterator){
		return fIterator->ent->d_type == DT_DIR;
	}
	return 1;
    //*/return provided_FolderIterator_isDir(fIterator);
}

void IMPLEMENT(FolderIterator_next)(FolderIterator *fIterator)
{
	if(fIterator->skipSpecials == 1){
		fIterator->ent = readdir(fIterator->dir);	 
		while(fIterator->ent != NULL && (stringCompare(fIterator->ent->d_name, ".") == 0 || stringCompare(fIterator->ent->d_name, "..") == 0)){
			fIterator->ent = readdir(fIterator->dir);
		}
	}
	else{
		fIterator->ent = readdir(fIterator->dir);
	}
    //*/provided_FolderIterator_next(fIterator);
}

FileIterator* IMPLEMENT(FileIterator_new)(FILE *file)
{    
	FileIterator *fIterator= malloc(sizeof(FileIterator));
	if(fIterator){
		if(FileIterator_init(fIterator, file)){
			free(fIterator);
			fIterator = NULL;
		}
	}
    return fIterator;
    //return provided_FileIterator_new(file);
}

int IMPLEMENT(FileIterator_init)(FileIterator *fIterator, FILE *file)
{
	if(fIterator && file){
		fIterator->file = file;
		fIterator->current = NULL;
		FileIterator_next(fIterator);
		return 0;
	}
	return 1;
    //return provided_FileIterator_init(fIterator, file);
}

void IMPLEMENT(FileIterator_delete)(FileIterator *fIterator)
{
	if(fIterator){
		FileIterator_finalize(fIterator);
		free(fIterator);
	}
    //provided_FileIterator_delete(fIterator);
}

void IMPLEMENT(FileIterator_finalize)(FileIterator *fIterator)
{
	fIterator->file = NULL;
	free(fIterator->current);// = NULL;
	//provided_FileIterator_finalize(fIterator);
}

int IMPLEMENT(FileIterator_hasNext)(const FileIterator *fIterator)
{
	return fIterator->current != NULL;
    //return provided_FileIterator_hasNext(fIterator);
}

const char* IMPLEMENT(FileIterator_get)(const FileIterator *fIterator)
{
	return fIterator->current;
    //return provided_FileIterator_get(fIterator);
}

void IMPLEMENT(FileIterator_next)(FileIterator *fIterator)
{
	if(fIterator){
		size_t size = 0;
		char * line = NULL;
		int result = getline(&line, &size, fIterator->file);
		if(result == -1){
			free(fIterator->current);
			fIterator->current = NULL;
		}
		else{
			char * tmpInutile = NULL;
			char * tmp = getRealString(line, '#', &tmpInutile);
			if(tmpInutile){
				*tmpInutile = '\0';
			}
			else{
				char * pos = (char * ) findFirst(tmp, "\n");
				if(pos){
					*pos = '\0';
				}
			}
			free(fIterator->current);
			fIterator->current = duplicateString(tmp);
			free(tmp);
		}
		if(line){
			free(line);
		}
	}
	//*/provided_FileIterator_next(fIterator);
}
