#ifndef COMMAND_H

/*-------------------------------------------------------------------------*
 | Copyright (C) 2018 Département Informatique de PolyTech Tours.          |
 |                                                                         |
 | This file is part of PolyShell, yet another shell.                      |
 |                                                                         |
 | PolyShell is free software; you can redistribute it and/or modify       |
 | it under the terms of the GNU General Public License as published by    |
 | the Free Software Foundation; either version 3 of the License,          |
 | or (at your option) any later version.                                  |
 |                                                                         |
 | PolyShell is distributed in the hope that it will be useful,            |
 | but WITHOUT ANY WARRANTY; without even the implied warranty of          |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            |
 | GNU General Public License for more details.                            |
 |                                                                         |
 | You should have received a copy of the GNU General Public License       |
 | along with this program. If not, see <http://www.gnu.org/licenses/>.    |
 |                                                                         |
 | Additional permission under GNU GPL version 3 section 7 ---- If you     |
 | modify PolyShell, or any covered work, by linking or combining it with  |
 | libprovided (or a modified version of that library), containing parts   |
 | covered by the terms of the Creative Commons BY-NC-ND 4.0 International |
 | License, the licensors of  PolyShell grant you additional permission    |
 | to convey the resulting work.                                           |
 *-------------------------------------------------------------------------*/

#define COMMAND_H

//
#include "misc/overridable.h"

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

#include <stdio.h>

/**
 *
 */
int run(const char *command);

/**
 *
 */
int script(FILE *file);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

/**
 *
 */
#define INTERNAL_WATCH "watchdog"

/**
 *
 */
#define INTERNAL_CHDIR "cd"

/**
 *
 */
#define INTERNAL_NEXIT "exit"

/**
 *
 */
#define INTERNAL_CEXIT "clear_exit"

/**
 *
 */
#define INTERNAL_FHIST "history"

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

typedef enum {

    //
    NORMAL,

    //
    APPEND,

    //
    FUSION,

    //
    UNDEFINED

} RedirectionType;

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

/**
 *
 */
typedef struct __Command__ {

    //
    unsigned int status;

    //
    char* base;

    //
    char** options;

    //
    unsigned int nbOptions;

    //
    unsigned int capacityOption;

    //
    char* redirections[3];

    //
    RedirectionType redirectionTypes[3];

    //
    struct __Command__ *next;

    //
    struct __Command__ *prev;

} Command;

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

/**
 *
 */
extern Command** livingCommands;

/**
 *
 */
extern unsigned int livingCommandsSize;

/**
 *
 */
extern unsigned int livingCommandsCapacity;

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

/**
 *
 */
LIB Command* OVERRIDABLE(Command_new)(const char *base);

/**
 *
 */
LIB int OVERRIDABLE(Command_init)(Command *cmd, const char *base);

/**
 *
 */
LIB void OVERRIDABLE(Command_delete)(Command *cmd);

/**
 *
 */
LIB void OVERRIDABLE(Command_finalize)(Command *cmd);

/**
 *
 */
int Command_addLivingCommand(Command *ncommand);

/**
 *
 */
void Command_flushLivingCommands(void);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

/**
 *
 */
LIB Command* OVERRIDABLE(Command_redirect)(Command *cmd, int fd, const char *filename);

/**
 *
 */
LIB Command* OVERRIDABLE(Command_appendRedirect)(Command *cmd, int fd, const char *filename);

/**
 *
 */
LIB Command* OVERRIDABLE(Command_mergeOutputs)(Command *cmd);

/**
 *
 */
LIB Command* OVERRIDABLE(Command_pipe)(Command *c1, Command *c2);

/**
 *
 */
LIB Command* OVERRIDABLE(Command_addOption)(Command *cmd, const char *arg, int expend);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

/**
 *
 */
LIB int OVERRIDABLE(Command_execute)(Command *cmd);

/**
 *
 */
int Command_unparse(const Command *cmd);

/**
 *
 */
LIB size_t OVERRIDABLE(Command_getNbMember)(const Command *cmd);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

//
#include "impl/command.h"

#endif
