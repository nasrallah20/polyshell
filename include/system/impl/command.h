/*-------------------------------------------------------------------------*
 | Copyright (C) 2018 Département Informatique de PolyTech Tours.          |
 |                                                                         |
 | This file is part of PolyShell, yet another shell.                      |
 |                                                                         |
 | PolyShell is free software; you can redistribute it and/or modify       |
 | it under the terms of the GNU General Public License as published by    |
 | the Free Software Foundation; either version 3 of the License,          |
 | or (at your option) any later version.                                  |
 |                                                                         |
 | PolyShell is distributed in the hope that it will be useful,            |
 | but WITHOUT ANY WARRANTY; without even the implied warranty of          |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            |
 | GNU General Public License for more details.                            |
 |                                                                         |
 | You should have received a copy of the GNU General Public License       |
 | along with this program. If not, see <http://www.gnu.org/licenses/>.    |
 |                                                                         |
 | Additional permission under GNU GPL version 3 section 7 ---- If you     |
 | modify PolyShell, or any covered work, by linking or combining it with  |
 | libprovided (or a modified version of that library), containing parts   |
 | covered by the terms of the Creative Commons BY-NC-ND 4.0 International |
 | License, the licensors of  PolyShell grant you additional permission    |
 | to convey the resulting work.                                           |
 *-------------------------------------------------------------------------*/

// #########################################################################
// ###################### DO NEVER INCLUDE THIS FILE #######################
// #########################################################################

Command* provided_Command_new(const char *base);
Command* user_Command_new(const char *base);

int provided_Command_init(Command *cmd, const char *base);
int user_Command_init(Command *cmd, const char *base);

void provided_Command_delete(Command *cmd);
void user_Command_delete(Command *cmd);

void provided_Command_finalize(Command *cmd);
void user_Command_finalize(Command *cmd);

Command* provided_Command_redirect(Command *cmd, int fd, const char *filename);
Command* user_Command_redirect(Command *cmd, int fd, const char *filename);

Command* provided_Command_appendRedirect(Command *cmd, int fd, const char *filename);
Command* user_Command_appendRedirect(Command *cmd, int fd, const char *filename);

Command* provided_Command_mergeOutputs(Command *cmd);
Command* user_Command_mergeOutputs(Command *cmd);

Command* provided_Command_pipe(Command *c1, Command *c2);
Command* user_Command_pipe(Command *c1, Command *c2);

Command* provided_Command_addOption(Command *cmd, const char *arg, int expend);
Command* user_Command_addOption(Command *cmd, const char *arg, int expend);

int provided_Command_execute(Command *cmd);
int user_Command_execute(Command *cmd);

size_t provided_Command_getNbMember(const Command *cmd);
size_t user_Command_getNbMember(const Command *cmd);
