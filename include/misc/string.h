#ifndef STRING_H

/*-------------------------------------------------------------------------*
 | Copyright (C) 2018 Département Informatique de PolyTech Tours.          |
 |                                                                         |
 | This file is part of PolyShell, yet another shell.                      |
 |                                                                         |
 | PolyShell is free software; you can redistribute it and/or modify       |
 | it under the terms of the GNU General Public License as published by    |
 | the Free Software Foundation; either version 3 of the License,          |
 | or (at your option) any later version.                                  |
 |                                                                         |
 | PolyShell is distributed in the hope that it will be useful,            |
 | but WITHOUT ANY WARRANTY; without even the implied warranty of          |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            |
 | GNU General Public License for more details.                            |
 |                                                                         |
 | You should have received a copy of the GNU General Public License       |
 | along with this program. If not, see <http://www.gnu.org/licenses/>.    |
 |                                                                         |
 | Additional permission under GNU GPL version 3 section 7 ---- If you     |
 | modify PolyShell, or any covered work, by linking or combining it with  |
 | libprovided (or a modified version of that library), containing parts   |
 | covered by the terms of the Creative Commons BY-NC-ND 4.0 International |
 | License, the licensors of  PolyShell grant you additional permission    |
 | to convey the resulting work.                                           |
 *-------------------------------------------------------------------------*/

#define STRING_H

//
#include "overridable.h"

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

#include <stdlib.h>

/**
 *
 */
LIB char OVERRIDABLE(toLowerCase)(char c);

/**
 *
 */
LIB char OVERRIDABLE(toUpperCase)(char c);

/**
 *
 */
LIB const char* OVERRIDABLE(indexOfString)(const char *foin, const char *aiguille, int csensitive);

/**
 *
 */
LIB size_t OVERRIDABLE(stringLength)(const char *str);

/**
 *
 */
LIB char* OVERRIDABLE(duplicateString)(const char *str);

/**
 *
 */
LIB const char* OVERRIDABLE(findFirst)(const char *str, const char *separators);

/**
 *
 */
LIB char* OVERRIDABLE(findLast)(char *str, char c);

/**
 *
 */
LIB int OVERRIDABLE(stringCompare)(const char *str1, const char *str2);

/**
 *
 */
LIB char* OVERRIDABLE(concatenateStrings)(const char *str1, const char *str2, int minDestSize);

/**
 *
 */
#define NO_HINT -1

/**
 *
 */
LIB void OVERRIDABLE(copyStringWithLength)(char *dest, const char * src, size_t destSize);

/**
 *
 */
LIB char* OVERRIDABLE(mkReverse)(char *str, int lengthHint);

/**
 *
 */
LIB const char* OVERRIDABLE(startWith)(const char *str, const char *start);

/**
 *
 */
LIB int OVERRIDABLE(belongs)(char c, const char *str);

/**
 *
 */
LIB char* OVERRIDABLE(subString)(const char *start, size_t length);

/**
 *
 */
LIB void OVERRIDABLE(mkCommon)(char *result, const char *str);

/**
 *
 */
LIB int OVERRIDABLE(isNotEmpty)(const char *str);

/**
 *
 */
LIB char* OVERRIDABLE(getProtString)(const char *str, char c);

/**
 *
 */
LIB char* OVERRIDABLE(getRealString)(const char *str, char c, char **firstNotEscaped);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

/**
 *
 */
typedef struct {

    //
    const char *str;

    //
    const char *separators;

    //
    const char *next;

} Tokenizer;

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

/**
 *
 */
LIB Tokenizer* OVERRIDABLE(Tokenizer_new)(const char *str, const char *separators);

/**
 *
 */
LIB int OVERRIDABLE(Tokenizer_init)(Tokenizer *tokenizer, const char *str, const char *separators);

/**
 *
 */
LIB void OVERRIDABLE(Tokenizer_delete)(Tokenizer *tokenizer);

/**
 *
 */
LIB void OVERRIDABLE(Tokenizer_finalize)(Tokenizer *tokenizer);

/**
 *
 */
LIB int OVERRIDABLE(Tokenizer_hasNext)(const Tokenizer *tokenizer);

/**
 *
 */
LIB char* OVERRIDABLE(Tokenizer_get)(const Tokenizer *tokenizer);

/**
 *
 */
LIB void OVERRIDABLE(Tokenizer_next)(Tokenizer *tokenizer);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

//
#include "impl/string.h"

#endif
